﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebPageParser_Module.BaseClasses;
using WebPageParser_Module.Notifications;

namespace WebPageParser_Module.DataCollectors
{
    public delegate void DataLoadFinished(DataLoadType dataLoadType, clsOntologyItem resultLoad);
    public enum DataLoadType
    {
        Url = 0,
        TextParser = 1,
        WebPagerParserConfig = 2
    }
    public class DataCollector_WebPageParser : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyAppDBConnector.OntologyModDBConnector dbReader_WebPageParserConfig;
        private OntologyAppDBConnector.OntologyModDBConnector dbReader_WebPageParserConfig_Relations;
        private Thread textPageParserThread;

        public event DataLoadFinished dataLoadFinished;

        private List<clsOntologyItem> urls;
        public List<clsOntologyItem> Urls
        {
            get { return urls; }
            set
            {
                urls = value;
                RaisePropertyChanged(NotifyChanges.DataCollectorUrls_Urls);
            }
        }

        private List<clsOntologyItem> textParsers;
        public List<clsOntologyItem> TextParsers
        {
            get { return textParsers; }
            set
            {
                textParsers = value;
                RaisePropertyChanged(NotifyChanges.DataCollectorUrls_TextParsers);
            }
        }

        public clsOntologyItem GetUriOfTextParser(clsOntologyItem textParser)
        {
            
            var relation = dbReader_WebPageParserConfig_Relations.ObjectRels.FirstOrDefault(rel => rel.ID_Other == textParser.GUID);
            if (relation != null)
            {
                var urlItem = dbReader_WebPageParserConfig_Relations.ObjectRels.FirstOrDefault(rel => rel.ID_Object == relation.ID_Object && rel.ID_Parent_Other == localConfig.OItem_class_url.GUID);

                return new clsOntologyItem
                {
                    GUID = urlItem.ID_Other,
                    Name = urlItem.Name_Other,
                    GUID_Parent = urlItem.ID_Parent_Other,
                    Type = urlItem.Ontology
                };                
            }
            else
            {
                return null;
            }


        }

        public clsOntologyItem StartLoadWebPageParserModule()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            try
            {
                textPageParserThread.Abort();
            }
            catch (Exception ex)
            {

            }


            textPageParserThread = new Thread(LoadData);
            textPageParserThread.Start();

            
            return result;
        }

        private void LoadData()
        {
            var result = Load001_TextPageParsers();

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = Load002_Rels();
                if (dataLoadFinished != null)
                {
                    dataLoadFinished(DataLoadType.TextParser | DataLoadType.Url, result);
                }
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                if (dataLoadFinished != null)
                {
                    dataLoadFinished(DataLoadType.WebPagerParserConfig, result);
                }
            }

        }

        private clsOntologyItem Load001_TextPageParsers()
        {
            var searchTextPageConfig = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID_Parent = localConfig.OItem_class_webpageparser_module.GUID
                }
            };

            var result = dbReader_WebPageParserConfig.GetDataObjects(searchTextPageConfig);

            return result;
        }

        private clsOntologyItem Load002_Rels()
        {

            var searchRels = dbReader_WebPageParserConfig.Objects1.Select(config => new clsObjectRel
            {
                ID_Object = config.GUID,
                ID_RelationType = localConfig.OItem_relationtype_url__start_with_.GUID,
                ID_Parent_Other = localConfig.OItem_class_url.GUID
            }).ToList();

            searchRels.AddRange(dbReader_WebPageParserConfig.Objects1.Select(config => new clsObjectRel
            {
                ID_Object = config.GUID,
                ID_RelationType = localConfig.OItem_relationtype_uses.GUID,
                ID_Parent_Other = localConfig.OItem_class_textparser.GUID
            }));

            var result = dbReader_WebPageParserConfig_Relations.GetDataObjectRel(searchRels);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                Urls = (from relation in dbReader_WebPageParserConfig_Relations.ObjectRels.Where(rel => rel.ID_Parent_Other == localConfig.OItem_class_url.GUID)
                        group relation by new
                        {
                            GUID_Url = relation.ID_Other,
                            Name_Url = relation.Name_Other
                        }
                        into relations
                        select new clsOntologyItem
                        {
                            GUID = relations.Key.GUID_Url,
                            Name = relations.Key.Name_Url,
                            GUID_Parent = localConfig.OItem_class_url.GUID,
                            Type = localConfig.Globals.Type_Object
                        }).ToList();

                TextParsers = (from relation in dbReader_WebPageParserConfig_Relations.ObjectRels.Where(rel => rel.ID_Parent_Other == localConfig.OItem_class_textparser.GUID)
                               group relation by new
                               {
                                   GUID_TextParser = relation.ID_Other,
                                   Name_TextParser = relation.Name_Other
                               }
                        into relations
                               select new clsOntologyItem
                               {
                                   GUID = relations.Key.GUID_TextParser,
                                   Name = relations.Key.Name_TextParser,
                                   GUID_Parent = localConfig.OItem_class_textparser.GUID,
                                   Type = localConfig.Globals.Type_Object,
                                   Mark = false
                                }).ToList();

            }
            else
            {
                Urls = new List<clsOntologyItem>();
                TextParsers = new List<clsOntologyItem>();
            }

            return result;
        }
        
        public DataCollector_WebPageParser(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }   
        
        private void Initialize()
        {
            dbReader_WebPageParserConfig = new OntologyAppDBConnector.OntologyModDBConnector(localConfig.Globals);
            dbReader_WebPageParserConfig_Relations = new OntologyAppDBConnector.OntologyModDBConnector(localConfig.Globals);
        }    
    }
}
