﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPageParser_Module.BaseClasses;
using WebPageParser_Module.Notifications;

namespace WebPageParser_Module
{
    public class MainWindowViewModel : ViewModelBase
    {
        private string label_WebPage;
        public string Label_WebPage
        {
            get { return label_WebPage; }
            set
            {
                if (label_WebPage == value) return;
                label_WebPage = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_Label_WebPage);
            }
        }

        private List<clsOntologyItem> urlItems;
        public List<clsOntologyItem> UrlItems
        {
            get { return urlItems; }
            set
            {
                urlItems = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_UrlItems);
            }
        }

        private clsOntologyItem loadResult_Urls;
        public clsOntologyItem LoadResult_Urls
        {
            get { return loadResult_Urls; }
            set
            {
                loadResult_Urls = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_LoadResult_Urls);
            }
        }

        private string text_Url;
        public string Text_Url
        {
            get { return text_Url; }
            set
            {
                if (text_Url == value) return;
                text_Url = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_Text_Url);

            }
        }

        private string label_TextParser;
        public string Label_TextParser
        {
            get
            {
                
                return label_TextParser;
            }
            set
            {
                if (label_TextParser == value) return;
                label_TextParser = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_Label_TextParser);
            }
        }

        private List<clsOntologyItem> textParserItems;
        public List<clsOntologyItem> TextParserItems
        {
            get { return textParserItems; }
            set
            {
                textParserItems = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_TextParserItems);
            }
        }

        private clsOntologyItem loadResult_TextParserItems;
        public clsOntologyItem LoadResult_TextParserItems
        {
            get { return loadResult_TextParserItems; }
            set
            {
                loadResult_TextParserItems = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_LoadResult_TextParserItems);
            }
        }

        private clsOntologyItem selectedTextParser;
        public clsOntologyItem SelectedTextParser
        {
            get { return selectedTextParser; }
            set
            {
                selectedTextParser = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_LoadResult_SelectedTextParser);
            }
        }

        private string lastErrorMessage;
        public string LastErrorMessage
        {
            get { return lastErrorMessage; }
            set
            {
                lastErrorMessage = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_LastErrorMessage);
            }
        }

        private string label_SaveHtml;
        public string Label_SaveHtml
        {
            get { return label_SaveHtml; }
            set
            {
                if (label_SaveHtml == value) return;
                label_SaveHtml = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_Label_SaveHtml);
            }
        }

        private bool isEnabled_SaveHtml;
        public bool IsEnabled_SaveHtml
        {
            get { return isEnabled_SaveHtml; }
            set
            {
                if (isEnabled_SaveHtml == value) return;
                isEnabled_SaveHtml = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_IsEnabled_SaveHtml);

            }
        }

        private string label_OpenParser;
        public string Label_OpenParser
        {
            get { return label_OpenParser; }
            set
            {
                if (label_OpenParser == value) return;
                label_OpenParser = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_Label_OpenParser);
            }
        }

        private bool isEnabled_OpenParser;
        public bool IsEnabled_OpenParser
        {
            get { return isEnabled_OpenParser; }
            set
            {
                if (isEnabled_OpenParser == value) return;
                isEnabled_OpenParser = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_IsEnabled_OpenParser);

            }
        }

        private string navigatedUrl;
        public string NavigatedUrl
        {
            get { return navigatedUrl; }
            set
            {
                navigatedUrl = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_NavigatedUrl);
            }
        }

        private string docHtml;
        public string DocHtml
        {
            get { return docHtml; }
            set
            {
                docHtml = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_DocHtml);
            }
        }

        private string pathFile = @"%TEMP%\" + Guid.NewGuid().ToString() + ".html";
        public string PathFile
        {
            get { return pathFile; }
            set
            {
                pathFile = value;
                RaisePropertyChanged(NotifyChanges.MainWindow_PathFile);
            }
        }


    }
}
