﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace WebPageParser_Module.HelperConverter
{
    public class LoadResultToVisibilityConverter : IValueConverter
    {
        private clsLogStates logStates = new clsLogStates();

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return Visibility.Hidden;

            if (value is clsOntologyItem)
            {
                var result = (clsOntologyItem)value;

                if (result.GUID == logStates.LogState_Nothing.GUID)
                {
                    return Visibility.Visible;
                }

            }

            return Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return logStates.LogState_Error.Clone();
            if (!(value is Visibility)) return logStates.LogState_Error.Clone();

            var visibility = (Visibility)value;
            if (visibility == Visibility.Visible)
            {
                return logStates.LogState_Nothing.Clone();
            }
            else
            {
                return logStates.LogState_Success.Clone();
            }
            
        }
    }
}
