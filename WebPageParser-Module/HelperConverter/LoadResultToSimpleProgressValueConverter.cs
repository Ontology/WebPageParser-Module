﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace WebPageParser_Module.HelperConverter
{
    public class LoadResultToSimpleProgressValueConverter : IValueConverter
    {
        private clsLogStates logStates = new clsLogStates();

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return false;

            if (value is clsOntologyItem)
            {
                var result = (clsOntologyItem)value;

                if (result.GUID == logStates.LogState_Nothing.GUID)
                {
                    return 50;
                }
                else
                {
                    return 0;
                }

            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return logStates.LogState_Error.Clone();

            int iValue = 0;
            if (int.TryParse(value.ToString(), out iValue))
            {
                if (iValue > 0)
                {
                    return logStates.LogState_Nothing.Clone();
                }
                else
                {
                    return logStates.LogState_Success.Clone();
                }
            }

            return logStates.LogState_Error.Clone();
        }
    }
}
