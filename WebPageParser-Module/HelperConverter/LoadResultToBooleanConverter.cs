﻿using OntologyClasses.BaseClasses;
using OntologyClasses.DataClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace WebPageParser_Module.HelperConverter
{
    public class LoadResultToBooleanConverter :  IValueConverter
    {
        private clsLogStates logStates = new clsLogStates();

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return false;

            if (value is clsOntologyItem)
            {
                var result = (clsOntologyItem)value;

                return result.GUID == logStates.LogState_Success.GUID;
              
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return logStates.LogState_Error.Clone();

            bool bValue = false;
            if (bool.TryParse(value.ToString(),out bValue))
            {
                if (bValue)
                {
                    return logStates.LogState_Success.Clone();
                }
                else
                {
                    return logStates.LogState_Nothing.Clone();
                }
            }

            return bValue;
        }
    }
}
