﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using mshtml;
using TextParser;

namespace WebPageParser_Module
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModelController viewModelController;
        private frmTextParser frmTextParser;

        public MainWindow()
        {
            InitializeComponent();
            viewModelController = (MainWindowViewModelController)DataContext;
            viewModelController.navigateTo += ViewModelController_navigateTo;
            viewModelController.openTextParser += ViewModelController_openTextParser;
            viewModelController.saveFile += ViewModelController_saveFile;
            viewModelController.InitializeViewModel();
            
        }


       
        private void ViewModelController_saveFile()
        {
            mshtml.HTMLDocument doc = (mshtml.HTMLDocument)WebBrowserDest.Document;
            var htmlText = doc.documentElement.innerHTML;
            File.WriteAllText(viewModelController.PathFile, htmlText);



            //dynamic htmlDoc = (mshtml.IHTMLDocument2)WebBrowserDest.Document;
            ///*webBrowser1 is the WebBrowser Control showing your page*/
            ////viewModelController.DocHtml = doc.documentElement.InnerHtml;
            //viewModelController.DocHtml = htmlDoc.documentElement.InnerHtml;
        }

        
        private void ViewModelController_savedFile()
        {

            
            
        }

        private void ViewModelController_openTextParser(OntologyClasses.BaseClasses.clsOntologyItem textParser)
        {
            if (frmTextParser == null)
            {
                frmTextParser = new frmTextParser(viewModelController.SelectedTextParser, viewModelController.PathFile);
                WindowInteropHelper wih = new WindowInteropHelper(this);

                frmTextParser.Show();
            }
        }

        private void ViewModelController_navigateTo(string url)
        {
            WebBrowserDest.Navigate(url);
        }

        private void comboBox_TextInput(object sender, TextCompositionEventArgs e)
        {
            viewModelController.SearchUrls(e.Text);
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            viewModelController.CheckNavigate(e.Key);
        }

        private void WebBrowserDest_Navigated(object sender, NavigationEventArgs e)
        {
            viewModelController.NavigatedUrl = e.Uri.AbsoluteUri;
        }

        private void buttonOpenParser_Click(object sender, RoutedEventArgs e)
        {
            viewModelController.OpenTextParser();
        }

        private void buttonSaveHtml_Click(object sender, RoutedEventArgs e)
        {
           
            viewModelController.SaveHtml();
        }

        private void textBoxUrl_TextChanged(object sender, TextChangedEventArgs e)
        {
            viewModelController.IsEnabled_SaveHtml = false;
        }

        private void WebBrowserDest_LoadCompleted(object sender, NavigationEventArgs e)
        {
            viewModelController.IsEnabled_SaveHtml = true;
        }
    }
}
