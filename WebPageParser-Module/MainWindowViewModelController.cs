﻿using Microsoft.Win32;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using WebPageParser_Module.DataCollectors;
using WebPageParser_Module.Notifications;

namespace WebPageParser_Module
{
    public delegate void NavigateTo(string url);
    public delegate void OpenTextParser(clsOntologyItem textParser);
    public delegate void SavedFile();
    public delegate void SaveFile();

    public class MainWindowViewModelController : MainWindowViewModel
    {
        public event NavigateTo navigateTo;
        public event OpenTextParser openTextParser;
        public event SaveFile saveFile;
        public event SavedFile savedFile;

        private DataCollector_WebPageParser dataCollector_Urls;

        private List<clsOntologyItem> urlItemsBase;

        private clsOntologyItem selectedUrlItem;

        

        private clsLocalConfig localConfig;
        public clsLocalConfig LocalConfig
        {
            get { return localConfig; }
            set
            {
                localConfig = value;
            }
        }

        public MainWindowViewModelController()
        {

            LocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (LocalConfig == null)
            {
                LocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(LocalConfig);
            }
        }

        public void InitializeViewModel()
        {
            Label_WebPage = "Adresse:";
            Label_TextParser = "Textparser:";
            Label_SaveHtml = "Save";
            Label_OpenParser = "Parser...";

            IsEnabled_OpenParser = false;
            PathFile = Environment.ExpandEnvironmentVariables(PathFile);
            LoadResult_Urls = localConfig.Globals.LState_Nothing.Clone();
            PropertyChanged += MainWindowViewModelController_PropertyChanged;
            dataCollector_Urls = new DataCollector_WebPageParser(localConfig);
            dataCollector_Urls.dataLoadFinished += DataCollector_Urls_dataLoadFinished;
            LoadResult_Urls = dataCollector_Urls.StartLoadWebPageParserModule();
            LoadResult_TextParserItems = LoadResult_Urls;
            var targetApplication = Process.GetCurrentProcess().ProcessName + ".exe";
            int ie_emulation = 10000;
            try
            {
                string tmp = Properties.Settings.Default.ie_emulation;
                ie_emulation = int.Parse(tmp);
            }
            catch { }

            SetIEVersioneKeyforWebBrowserControl(targetApplication, ie_emulation);

        }

        private void DataCollector_Urls_dataLoadFinished(DataLoadType dataLoadType, OntologyClasses.BaseClasses.clsOntologyItem resultLoad)
        {
            if (dataLoadType.HasFlag(DataLoadType.Url))
            {
                LoadResult_Urls = resultLoad;
                urlItemsBase = dataCollector_Urls.Urls;
            }
            if (dataLoadType.HasFlag(DataLoadType.TextParser))
            {
                LoadResult_TextParserItems = resultLoad;
                TextParserItems = dataCollector_Urls.TextParsers;
            }


        }

        private void MainWindowViewModelController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == NotifyChanges.MainWindow_LoadResult_SelectedTextParser)
            {
                selectedUrlItem = dataCollector_Urls.GetUriOfTextParser(SelectedTextParser);

                Uri webPageParserUri;

                if (Uri.TryCreate(selectedUrlItem.Name, UriKind.Absolute, out webPageParserUri))
                {
                    Text_Url = webPageParserUri.AbsoluteUri;
                    Navigate();
                }
                else
                {
                    Text_Url = "";
                }
                IsEnabled_OpenParser = true;
            }
            if (e.PropertyName == NotifyChanges.MainWindow_NavigatedUrl)
            {
                Text_Url = NavigatedUrl;
                
            }
            if (e.PropertyName == NotifyChanges.MainWindow_DocHtml)
            {
                if (File.Exists(PathFile))
                {
                    File.Delete(PathFile);
                }
                File.WriteAllText(PathFile, DocHtml);
                if (savedFile != null)
                {
                    savedFile();
                    
                }
            }
        }

        
        public void SearchUrls(string urlPart)
        {
            if (urlPart.Length <= 2) return;
            UrlItems = urlItemsBase.Where(urlItem => urlItem.Name.ToLower().Contains(urlPart.ToLower())).ToList();
        }

        public void CheckNavigate(Key key)
        {
            if (key == Key.Enter)
            {
                Navigate();   
            }
            
        }

        public void Navigate()
        {
            Uri uri = null;
            if (!Uri.TryCreate(Text_Url, UriKind.Absolute, out uri))
            {
                var regexUrl = new Regex(".*://");
                if (!regexUrl.Match(Text_Url).Success)
                {
                    Text_Url = "http://" + Text_Url;
                    Navigate();
                }
                return;
            }
            
            
            if (uri == null) return;

            if (navigateTo != null)
            {
                navigateTo(uri.AbsoluteUri);
            }
        }

        private void SetIEVersioneKeyforWebBrowserControl(string appName, int ieval)
        {
            RegistryKey Regkey = null;
            try
            {


                Regkey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", true);

                //If the path is not correct or 
                //If user't have priviledges to access registry 
                if (Regkey == null)
                {
                    return;
                }

                string FindAppkey = Convert.ToString(Regkey.GetValue(appName));

                //Check if key is already present 
                if (FindAppkey == "" + ieval)
                {
                    Regkey.Close();
                    return;
                }

                //If key is not present or different from desired, add/modify the key , key value 
                Regkey.SetValue(appName, unchecked((int)ieval), RegistryValueKind.DWord);

                //check for the key after adding 
                FindAppkey = Convert.ToString(Regkey.GetValue(appName));

                


            }
            catch (Exception ex)
            {
                LastErrorMessage = "Um die Kompatibilität des Internet-Explorers zu setzen, muss diese Anwendung administrativ gestartet werden.";
                return;

            }
            finally
            {
                //Close the Registry 
                if (Regkey != null)
                    Regkey.Close();
            }


        }

        public void OpenTextParser()
        {
            if (openTextParser != null)
            {
                openTextParser(SelectedTextParser);
            }
        }

        public void SaveHtml()
        {
            if (saveFile != null)
            {
                IsEnabled_SaveHtml = false;
                saveFile();
            }
        }
    }
}
