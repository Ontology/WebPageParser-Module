﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using OntologyClasses.Interfaces;
using System.Runtime.InteropServices;

namespace WebPageParser_Module
{
    public class clsLocalConfig : ILocalConfig
    {
    private const string cstrID_Ontology = "6d2cb074a18c448eb9148fcdec8572f2";
    private ImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

    public clsOntologyItem OItem_class_textparser { get; set; }
    public clsOntologyItem OItem_class_url { get; set; }
    public clsOntologyItem OItem_class_user_authentication { get; set; }
    public clsOntologyItem OItem_class_web_connection { get; set; }
    public clsOntologyItem OItem_class_webparser { get; set; }
        public clsOntologyItem OItem_class_webpageparser_module { get; set; }
        


        public clsOntologyItem OItem_relationtype_authorized_by { get; set; }
    public clsOntologyItem OItem_relationtype_belongs_to { get; set; }
    public clsOntologyItem OItem_relationtype_connect_to { get; set; }
    public clsOntologyItem OItem_relationtype_needs { get; set; }
        public clsOntologyItem OItem_relationtype_uses { get; set; }
        public clsOntologyItem OItem_relationtype_url__start_with_ { get; set; }


        private void get_Data_DevelopmentConfig()
    {
        var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

        var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
        if (objOItem_Result.GUID == Globals.LState_Success.GUID)
        {
            if (objDBLevel_Config1.ObjectRels.Any())
            {

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                }).ToList();

                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingClass.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingObject.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                }));

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                {
                    if (!objDBLevel_Config2.ObjectRels.Any())
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }

        }

    }

    public clsLocalConfig()
    {
        Globals = new Globals();
        set_DBConnection();
        get_Config();
    }

    public clsLocalConfig(Globals Globals)
    {
        this.Globals = Globals;
        set_DBConnection();
        get_Config();
    }

    private void set_DBConnection()
    {
        objDBLevel_Config1 = new OntologyModDBConnector(Globals);
        objDBLevel_Config2 = new OntologyModDBConnector(Globals);
        objImport = new ImportWorker(Globals);
    }

    private void get_Config()
    {
        try
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();
        }
        catch (Exception ex)
        {
            var objAssembly = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            var strTitle = "Unbekannt";
            if (objCustomAttributes.Length == 1)
            {
                strTitle = objCustomAttributes.First().Title;
            }
            
            var objOItem_Result = objImport.ImportTemplates(objAssembly);
            if (objOItem_Result.GUID != Globals.LState_Error.GUID)
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            else
            {
                throw new Exception("Config not importable");
            }
            
        }
    }

    private void get_Config_AttributeTypes()
    {

    }

    private void get_Config_RelationTypes()
    {
            var objOList_relationtype_uses = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_uses".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

            if (objOList_relationtype_uses.Any())
            {
                OItem_relationtype_uses = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_uses.First().ID_Other,
                    Name = objOList_relationtype_uses.First().Name_Other,
                    GUID_Parent = objOList_relationtype_uses.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_url__start_with_ = (from objOItem in objDBLevel_Config1.ObjectRels
                                                          where objOItem.ID_Object == cstrID_Ontology
                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                          where objRef.Name_Object.ToLower() == "relationtype_url__start_with_".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                          select objRef).ToList();

            if (objOList_relationtype_url__start_with_.Any())
            {
                OItem_relationtype_url__start_with_ = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_url__start_with_.First().ID_Other,
                    Name = objOList_relationtype_url__start_with_.First().Name_Other,
                    GUID_Parent = objOList_relationtype_url__start_with_.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }


            var objOList_relationtype_authorized_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_authorized_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

        if (objOList_relationtype_authorized_by.Any())
        {
            OItem_relationtype_authorized_by = new clsOntologyItem()
            {
                GUID = objOList_relationtype_authorized_by.First().ID_Other,
                Name = objOList_relationtype_authorized_by.First().Name_Other,
                GUID_Parent = objOList_relationtype_authorized_by.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_belongs_to = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_belongs_to".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_belongs_to.Any())
        {
            OItem_relationtype_belongs_to = new clsOntologyItem()
            {
                GUID = objOList_relationtype_belongs_to.First().ID_Other,
                Name = objOList_relationtype_belongs_to.First().Name_Other,
                GUID_Parent = objOList_relationtype_belongs_to.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_connect_to = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "relationtype_connect_to".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                select objRef).ToList();

        if (objOList_relationtype_connect_to.Any())
        {
            OItem_relationtype_connect_to = new clsOntologyItem()
            {
                GUID = objOList_relationtype_connect_to.First().ID_Other,
                Name = objOList_relationtype_connect_to.First().Name_Other,
                GUID_Parent = objOList_relationtype_connect_to.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_relationtype_needs = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "relationtype_needs".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                           select objRef).ToList();

        if (objOList_relationtype_needs.Any())
        {
            OItem_relationtype_needs = new clsOntologyItem()
            {
                GUID = objOList_relationtype_needs.First().ID_Other,
                Name = objOList_relationtype_needs.First().Name_Other,
                GUID_Parent = objOList_relationtype_needs.First().ID_Parent_Other,
                Type = Globals.Type_RelationType
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Objects()
    {

    }

    private void get_Config_Classes()
    {
            var objOList_class_webpageparser_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "class_webpageparser_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                                       select objRef).ToList();

            if (objOList_class_webpageparser_module.Any())
            {
                OItem_class_webpageparser_module = new clsOntologyItem()
                {
                    GUID = objOList_class_webpageparser_module.First().ID_Other,
                    Name = objOList_class_webpageparser_module.First().Name_Other,
                    GUID_Parent = objOList_class_webpageparser_module.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_textparser = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "class_textparser".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

        if (objOList_class_textparser.Any())
        {
            OItem_class_textparser = new clsOntologyItem()
            {
                GUID = objOList_class_textparser.First().ID_Other,
                Name = objOList_class_textparser.First().Name_Other,
                GUID_Parent = objOList_class_textparser.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_url = (from objOItem in objDBLevel_Config1.ObjectRels
                                  where objOItem.ID_Object == cstrID_Ontology
                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                  where objRef.Name_Object.ToLower() == "class_url".ToLower() && objRef.Ontology == Globals.Type_Class
                                  select objRef).ToList();

        if (objOList_class_url.Any())
        {
            OItem_class_url = new clsOntologyItem()
            {
                GUID = objOList_class_url.First().ID_Other,
                Name = objOList_class_url.First().Name_Other,
                GUID_Parent = objOList_class_url.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_user_authentication = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "class_user_authentication".ToLower() && objRef.Ontology == Globals.Type_Class
                                                  select objRef).ToList();

        if (objOList_class_user_authentication.Any())
        {
            OItem_class_user_authentication = new clsOntologyItem()
            {
                GUID = objOList_class_user_authentication.First().ID_Other,
                Name = objOList_class_user_authentication.First().Name_Other,
                GUID_Parent = objOList_class_user_authentication.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_web_connection = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "class_web_connection".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

        if (objOList_class_web_connection.Any())
        {
            OItem_class_web_connection = new clsOntologyItem()
            {
                GUID = objOList_class_web_connection.First().ID_Other,
                Name = objOList_class_web_connection.First().Name_Other,
                GUID_Parent = objOList_class_web_connection.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_webparser = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "class_webparser".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

        if (objOList_class_webparser.Any())
        {
            OItem_class_webparser = new clsOntologyItem()
            {
                GUID = objOList_class_webparser.First().ID_Other,
                Name = objOList_class_webparser.First().Name_Other,
                GUID_Parent = objOList_class_webparser.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

        public string IdLocalConfig
        {
            get
            {
                var attrib =
                      Assembly.GetExecutingAssembly()
                          .GetCustomAttributes(true)
                          .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute)attrib).Value;
                }
                else
                {
                    return null;
                }
            }
        }
    }

}